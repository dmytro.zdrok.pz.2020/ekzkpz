﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kpzclientekz.Models
{
    public class Appointment
    {
        public int Id { get; set; }
        public int VetCardId { get; set; }
        public string? Date { get; set; }
        public string? Time { get; set; }
    }
}
