﻿using kpzclientekz.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace kpzclientekz
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<VetCard> VetCards { get; set; }
        public List<Appointment> Appointments { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            VetCards = new List<VetCard>();
            Appointments = new List<Appointment>();
            GetVetCards();
            GetAppointments();
        }

        private async void GetVetCards()
        {
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.GetStringAsync("https://localhost:7109/Vet/vetcards");
            VetCards = JsonConvert.DeserializeObject<List<VetCard>>(res);
        }

        private async void GetAppointments()
        {
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.GetStringAsync("https://localhost:7109/Vet/appointments");
            Appointments = JsonConvert.DeserializeObject<List<Appointment>>(res);
        }
        private void GetVetCardsButton(object sender, RoutedEventArgs e)
        {

            dataGrid1.ItemsSource = VetCards;
        }
        private void GetAppointmentsButton(object sender, RoutedEventArgs e)
        {
            dataGrid2.ItemsSource = Appointments;
        }

        private async void DeleteVetCardButton(object sender, RoutedEventArgs e)
        {
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.
                DeleteAsync($"https://localhost:7109/Vet/vetcards/{VetCards[dataGrid1.SelectedIndex].Id}");
            GetVetCards();
        }

        private async void dataGrid1_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.
                PostAsJsonAsync("https://localhost:7109/Vet/vetcards", VetCards[dataGrid1.SelectedIndex]);
            GetVetCards();
        }

        private async void DeleteAppointmentButton(object sender, RoutedEventArgs e)
        {
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.
                DeleteAsync($"https://localhost:7109/Vet/appointments/{Appointments[dataGrid2.SelectedIndex].Id}");
            GetAppointments();
        }

        private async void dataGrid2_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.
                PostAsJsonAsync("https://localhost:7109/Vet/appointments", Appointments[dataGrid2.SelectedIndex]);
            GetAppointments();
        }
    }
}
