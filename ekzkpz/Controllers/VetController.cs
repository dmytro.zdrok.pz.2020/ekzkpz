﻿using Microsoft.AspNetCore.Mvc;

namespace ekzkpz.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VetController : ControllerBase
    {
        [HttpGet("vetcards")]
        public ActionResult<List<VetCard>> GetVetCards()
        {
            VetContext vetContext = new VetContext();

            return Ok(vetContext.VetCards.ToList());
        }

        [HttpGet("vetcards/{id}")]
        public ActionResult<List<VetCard>> GetVetCard(int id)
        {

            VetContext vetContext = new VetContext();

            List<VetCard> vetCards = new List<VetCard>();
            vetCards.Add(vetContext.VetCards.ToList().Find(x => x.Id == id));


            if (vetCards.Count == 0)
            {
                return BadRequest($"No vet cards by id {id}");
            }

            return Ok(vetCards);
        }

        [HttpPost("vetcards")]
        public ActionResult<List<VetCard>> AddVetCard(VetCard vetCard)
        {
            VetContext vetContext = new VetContext();

            vetContext.VetCards.Add(vetCard);
            vetContext.SaveChanges();

            return Ok(vetContext.VetCards.ToList());
        }

        [HttpDelete("vetcards/{id}")]
        public ActionResult<List<VetCard>> DeleteVetCard(int id)
        {
            VetContext vetContext = new VetContext();

            VetCard vetCard = vetContext.VetCards.ToList().Find(x => x.Id == id);

            if (vetCard == null)
            {
                return BadRequest($"No vet cards by id {id}");
            }

            vetContext.VetCards.Remove(vetCard);
            vetContext.SaveChanges();

            return Ok(vetContext.VetCards.ToList());
        }
        //-0000000000000000000000000000000000000000000000000000000
        [HttpGet("appointments")]
        public ActionResult<List<VetCard>> GetAppointments()
        {
            VetContext vetContext = new VetContext();

            return Ok(vetContext.Appointments.ToList());
        }

        [HttpGet("appointments/{id}")]
        public ActionResult<List<VetCard>> GetAppointment(int id)
        {

            VetContext vetContext = new VetContext();

            List<Appointment> appointments = new List<Appointment>();
            appointments.Add(vetContext.Appointments.ToList().Find(x => x.Id == id));


            if (appointments.Count == 0)
            {
                return BadRequest($"No appointmens by id {id}");
            }

            return Ok(appointments);
        }

        [HttpPost("appointments")]
        public ActionResult<List<VetCard>> AddAppointment(Appointment appointment)
        {
            VetContext vetContext = new VetContext();

            vetContext.Appointments.Add(appointment);
            vetContext.SaveChanges();

            return Ok(vetContext.Appointments.ToList());
        }

        [HttpDelete("appointments/{id}")]
        public ActionResult<List<VetCard>> DeleteAppointment(int id)
        {
            VetContext vetContext = new VetContext();

            Appointment appointment = vetContext.Appointments.ToList().Find(x => x.Id == id);

            if (appointment == null)
            {
                return BadRequest($"No appointments by id {id}");
            }

            vetContext.Appointments.Remove(appointment);
            vetContext.SaveChanges();

            return Ok(vetContext.Appointments.ToList());
        }
    }
}
