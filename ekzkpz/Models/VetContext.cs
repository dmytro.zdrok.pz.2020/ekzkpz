﻿using Microsoft.EntityFrameworkCore;

namespace ekzkpz.Models
{
    public class VetContext : DbContext
    {
        public VetContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=laptop-r88stm5t;database=vetclinicdb;trusted_connection=true;TrustServerCertificate=True;");
            }
        }
        public VetContext(DbContextOptions<VetContext> options) : base(options) { }
        public virtual DbSet<VetCard> VetCards { get; set; } = null!;
        public virtual DbSet<Appointment> Appointments { get; set; } = null!;
    }
}
