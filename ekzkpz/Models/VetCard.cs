﻿namespace ekzkpz.Models
{
    [PrimaryKey(nameof(Id))]
    public class VetCard
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? AnimalType { get; set; }
        public string? OwnerName { get; set; }
        public string? DateOfBirth { get; set; }
        public string? Diagnosis { get; set; }
    }
    
}
