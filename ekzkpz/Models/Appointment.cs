﻿namespace ekzkpz.Models
{
    [PrimaryKey(nameof(Id))]
    public class Appointment
    {
        public int Id { get; set; }
        public int VetCardId { get; set; }
        public string? Date { get; set; }
        public string? Time { get; set; }
    }
}
